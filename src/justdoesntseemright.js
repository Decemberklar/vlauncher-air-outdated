var gameInstances = {
	
	[0] = {
		label : "user label",
		game  : 'core ideals',
		base_iwad : false, // (|| or path to iwad)
		base_dir  : false, // (|| or path to base mod directory)
		exe_path : 'string to exe path'
		modfolder : 'general mod location',
		cmdparams : 'user parameters to be thrown at the exe as well as the wads'
	}

}

var modInstances = {
	
	[0] = {  // matchs gameInstances...instance

		[0] = {

			name : 'wad name',
			files : 'array of wad files'

		},
		
		[1] = {

			name : 'wad name',
			files : 'array of wad files'

		} 

	}

}