var game_sets = {};

/* DooM Engine */
	game_sets['doom'] = {}

		game_sets['doom']['doom'] = {
			name : 'DooM',
			slug : 'doom',
			type : 'file',
			cmd  : '-iwad ',
			core : 'doom.wad' }

		game_sets['doom']['doom2'] = {
			name : 'DooM II: Hell On Earth',
			slug : 'doom2',
			type : 'file',
			cmd  : '-iwad ',
			core : 'doom2.wad' }

		game_sets['doom']['heretic'] = {
			name : 'Heretic: Shadow Of The Serpent Riders',
			slug : 'heretic',
			type : 'file',
			cmd  : '-iwad ',
			core : 'heretic.wad' }

		game_sets['doom']['hexen'] = {
			name : 'HeXen: Beyond Heretic',
			slug : 'hexen',
			type : 'file',
			cmd  : '-iwad ',
			core : 'hexen.wad' }

		game_sets['doom']['hacx'] = {
			name : 'HACX',
			slug : 'hacx',
			type : 'file',
			cmd  : '-iwad ',
			core : 'hacx.wad' }

		game_sets['doom']['chex'] = {
			name : 'Chex Quest',
			slug : 'chex',
			type : 'file',
			cmd  : '-iwad ',
			core : 'chex3.wad' }
			
		game_sets['doom']['strife'] = {
			name : 'Strife',
			slug : 'strife',
			type : 'file',
			cmd  : '-iwad ',
			core : 'strife1.wad' }


/* iDTech1 (Quake) Engine */
	game_sets['idtech2'] = {}

		game_sets['idtech2']['quake'] = {
			name : 'Quake',
			slug : 'quake',
			type : 'dir',
			cmd : '-game ',
			core : 'id1' }

		game_sets['idtech2']['hexen2'] = {
			name : 'HeXen II',
			slug : 'hexen2',
			type : 'dir',
			cmd  : '-game ',
			core : 'id1'
		}

		game_sets['idtech2']['quake2'] = {
			name : 'Quake II',
			slug : 'quake2',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'id2' }

		game_sets['idtech2']['heretic2'] = {
			name : 'Heretic II',
			slug : 'heretic2',
			type : 'dir',
			cmd  : '-game ',
			core : 'id2'
		}

		game_sets['idtech2']['sin'] = {
			name : 'SiN',
			slug : 'sin',
			type : 'dir',
			cmd  : '-game ',
			core : 'id2'
		}

		game_sets['idtech2']['kingpin'] = {
			name : 'Kingpin: Life Of Crime',
			slug : 'kingpin',
			type : 'dir',
			cmd  : '-game ',
			core : 'id2'
		}

		game_sets['idtech2']['daikatana'] = {
			name : 'Daikatana',
			slug : 'daikatana',
			type : 'dir',
			cmd  : '+set ',
			core : 'main'
		}

		game_sets['idtech2']['sof'] = {
			name : 'Soldier Of Fortune',
			slug : 'sof',
			type : 'dir',
			cmd  : '-game ',
			core : 'id1'
		}

		game_sets['idtech2']['warsow'] = {
			name : 'Warsow',
			slug : 'warsow',
			type : 'dir',
			cmd  : '-game ',
			core : 'main'
		}

/* iDTech2 (Quake2) Engine */	
	game_sets['idtech3'] = {}

		game_sets['idtech3']['quake3'] = {
			name : 'Quake III',
			slug : 'quake3',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech3']['stv'] = {
			name : 'Star Trek: Voyager - Elite Force',
			slug : 'stv',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech3']['rtocw'] = {
			name : 'Return To Castle Wolfenstein',
			slug : 'rtocw',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech3']['sof2'] = {
			name : 'Soldier Of Fortune II: Double Helix',
			slug : 'sof2',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech3']['swjk1'] = {
			name : 'Star Wars Jedi Knight: Jedi Academy',
			slug : 'swjk1',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech3']['swjk2'] = {
			name : 'Star Wars Jedi Knight II: Jedi Outcast',
			slug : 'swjk2',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech3']['cod'] = {
			name : 'Call Of Duty',
			slug : 'cod',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech3']['mohaa'] = {
			name : 'Medal Of Honor: Allied Assault',
			slug : 'mohaa',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech3'][''] = {
			name : '',
			slug : '',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }


/* iDTech3 (Quake3) Engine */
	game_sets['idtech4'] = {}
		game_sets['idtech4']['quake4'] = {
			name : 'Quake IV',
			slug : 'quake4',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech4']['doom3'] = {
			name : 'DooM III',
			slug : 'doom3',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech4']['prey'] = {
			name : 'PREY',
			slug : 'prey',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }

		game_sets['idtech4']['wolf9'] = {
			name : 'Wolfenstein (2009)',
			slug : 'wolf9',
			type : 'dir', 
			cmd  : '-game ', 
			core : 'base' }


/* Unreal (Gold) Engine */
	game_sets['unreal1'] = {}
		
		game_sets['unreal1']['unreal'] = {
			name : 'Unreal Gold',
			slug : 'unreal',
			type : '',
			cmd  : '',
			core : ''
		}

		game_sets['unreal1']['ut'] = {
			name : 'Unreal Tournament',
			slug : 'ut',
			type : '',
			cmd  : '',
			core : ''
		}

		game_sets['unreal1']['rune'] = {
			name : 'Rune: Halls of Valhalla',
			slug : 'rune',
			type : '',
			cmd  : '',
			core : ''
		}

		game_sets['unreal1']['deusex'] = {
			name : 'Deus Ex',
			slug : 'deusex',
			type : '',
			cmd  : '',
			core : ''
		}


/* Unreal 2 (UT) Engine */
	game_sets['unreal2'] = {}


/* Unreal 3 (UT2004) Engine */
	game_sets['unreal3'] = {}


/* Build (Duke) Engine */
	game_sets['build'] = {}

		game_sets['build']['duke3d'] = {
			name : 'Duke Nukem 3D',
			slug : 'dn3d',
			type : 'dir', 
			cmd  : '-j', 
			core : 'dukenukem.grp' }

		game_sets['build']['shadowwarrior'] = {
			name : '',
			slug : 'sw',
			type : '', 
			cmd  : '', 
			core : '' }

		game_sets['build']['blood'] = {
			name : '',
			slug : 'blood',
			type : '', 
			cmd  : '', 
			core : '' }

		game_sets['build']['rrampage'] = {
			name : '',
			slug : 'rrampage',
			type : '', 
			cmd  : '', 
			core : '' }

/*

		game_sets['engine']['game'] = {
			name : '',
			type : '', 
			cmd  : '', 
			core : '' }

	game_sets['engine']['game'] = {
		name : '', // Displayed Name
		type : '', // File Core or Directory Core
		cmd  : '', // .exe switch needed to launch mods/core
		core : '' } // the core core base files for this title (i.e. doom = doom.wad, quake = id1)
*/