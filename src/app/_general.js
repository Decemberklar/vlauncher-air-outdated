// Just a simple json message constructor to help keep things clean
function jsonMsg(bool, msg, data){
	return msg = {
		status : bool, message : msg, values : data
	}
}

// Return the count of an object literal
function objectLength(obj) {
	var res = 0;
	for(var prop in obj) {
		if(obj.hasOwnProperty(prop)) {
			res++
		}
	}
	return res;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// UI CONTROLS

// Bam, common task turned into a function
function hideShow(hide,show,idorc) { idorc = idorc || '#'; $(hide).hide(); $(idorc + show).show(); } // not even used?

// nativeWindow UI Listeners
function onClose(){ window.nativeWindow.close(); }
function onMinimize(){ window.nativeWindow.minimize(); }
function onMaximize(){ window.nativeWindow.maximize(); toggle_maxirest(); }
function onRestore(){ window.nativeWindow.restore(); toggle_maxirest(); }
function onNativeMove(){ nativeWindow.startMove(); }
function onResize(type) { nativeWindow.startResize(type); }
function toggle_maxirest(){ $('.wc-maxi').toggle(); $('.wc-rest').toggle(); }

///////////////////////////////////////////////////////////////////////////////////////////////////
// STORAGE FUNCTIONS
// LocalStorage Getter
function getStore(item) {
	var item = air.EncryptedLocalStore.getItem(item);
 	return item;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
// LocalStorage Setter
function setStore(item, value) {
	var bytes = new air.ByteArray();
	bytes.writeUTFBytes(value);
	air.EncryptedLocalStore.setItem(item, bytes);
	return;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
// LocalStorage Array PUSH
function setArray(item, value) {
	var pushable = getStore(item);
	pushable.push(value);
	setStore(item, pushable);
	return;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
// LocalStorage Array SPLICE
function popArray(item, key) {
	var poppable = getStore(item);
	poppable.splice(key, 1);
	setStore(item);
	return;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
// LocalStorage Item Delete : Single
function removeStore(item) {
	air.EncryptedLocalStore.removeItem(item);
	return true;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
// LocalStorage Complete Purge
function purgeStore() {
	air.EncryptedLocalStore.reset();
	return true;
}