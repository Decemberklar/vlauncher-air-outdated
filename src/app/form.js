$('body').on('click','input.engine', function(e) {

	var checkval = $(this).val().toLowerCase();

	$('.engine-checkboxes').toggle();
	$('.engine-div').toggle();
	$('.engine-div input').val(checkval);

	var games = game_sets[checkval];

	$.each(games, function( k, v ) {
		$('.game-form-listings')
			.append( '<li> ' +
					 '<a class="game-selection" data-engine="' + checkval + '" data-id="' + v.slug + '">' +
					 v.name +
					 '</a></li>'
					);
	});
})

$('body').on('click','.engine-return', function(e){
	$('.engine-checkboxes').toggle();
	$('.engine-div').toggle();
	$('input.engine-input').val('');
	$('input.engine').prop('checked', false);
	$('.game-form-listings').empty();
});

$('body').on('click','a.game-selection',function(e){

	var engine = $(this).data('engine');
	var game   = $(this).data('id').toLowerCase();

	air.trace( game_sets[engine][game].name )

});

$('body').on('click', 'input.exe-path', function(e){

	var path = selectExe();

});