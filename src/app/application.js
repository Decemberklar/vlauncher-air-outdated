var Application = function( parameters ) {

	var App = this;

	App.container = $('#app-content');

	// This Block is most likely irrelevant until the userClass comes.
	App.settings = {
		path : '',
		page : 'launcher',
		auth : false,
		akey : ''
	}


	/*================================================================
    	Initialization
    ================================================================*/
	App.init = function() {

		var user_settings = getStore('userSettings');

		if( ! user_settings ) {

			user_settings = App.settings;

		} 

		App.render( user_settings.page );

	} //end.Init()


	/*================================================================
    	Page Rendering
    ================================================================*/
	App.render = function( page ) {

		App.container.empty().addClass('lf');
		
		var pageContent = 'html/' + page + '.html';

		App.container.load(pageContent, function() {

			App.addListeners();
		
		}).removeClass('lf');

	} //end.Render()


	/*================================================================
    	Get Page Data
    ================================================================*/
	App.getData = function( type ) {

		if(type == 'gamelist') {

			//var gamelist = getStore('userInstallations');
			var gamelist = {}
			gamelist[0] = {'game':'doom'}
			gamelist[1] = {'game':'quake'}

			//air.trace( objectLength(gamelist) )

			if( gamelist ) {

				var returnData = jsonMsg(true, 'Games Found', gamelist);

			} else {

				var returnData = jsonMsg(false, 'No Games Found', null);

			}

			return JSON.stringify( returnData );
		}

	} //end.getData()


	/*================================================================
    	Add Listeners to Newly Rendered Pages
    ================================================================*/
	App.addListeners = function() {

		/*==========  Process Data Into Page  ==========*/
		$('div#section-data').each(function() {

			$(this).empty().addClass('lf');

			var base 	= $(this);
			var type    = $(this).data('type');
			var tar     = $(this).data('tar');
			var process = JSON.parse( App.getData(type) );

			// No Data Found
			if(process.status == false) {
			
				$(this).append('No Data Found. This may be an error in your <a data-page="configuration">configuration</a>.').removeClass('lf');
				return;
			
			// Data Found
			} else {
			
				// SECTION.TYPE.GAMELIST
				if(type == 'gamelist') {

					sendOff = {
						exepath : 'G:/Games/Source Ports/Zandronum/Zandronum.exe',
						iwad	: 'DOOM.WAD',
						params  : ''
					}

					base.append('<ul class="game-listings"></ul>');

					$.each(process.values, function( k, v ){

						$('.game-listings').append(

							'<li>' + v.game +
							'<a class="batch" data-exe="' + sendOff.exepath + '">&#xF16B;</a>' +
							'<a class="batch">&#xF0A9;</a>' +
							'<a class="batch" data-page="edit-form" data-id="' + k + '">&#xf04D;</a>' + 
							'</li>'

						);

					});
				
					$(this).removeClass('lf');

				} //end.type.gamelist

			}

		});

		/*==========  Start Listening For New Link Clicks  ==========*/
		$('a').click(function(){
			
			// Page Link Listeners
			var requestPage = $(this).data('page');
			if(requestPage) {
				App.render(requestPage); return;
			} 

			// Game Launch Listeners
			var gamelaunch = $(this).data('launch');
			if(gamelaunch) {
				launchGame( gamelaunch );
			}

		});

	} //end.addListeners()
	

	// Initialize Application
	App.init();

}