/*================================================================
	Game Launcher (a.k.a where the magic happens)
================================================================*/
function launchGame(data) {

	var process = new air.NativeProcess();
	var startup = new air.NativeProcessStartupInfo();
	var exe = new air.File();
	exe.nativePath = data; // The nativePath is what will be stored into the .LS, note it.

	var params  = new air.Vector["<String>"]();
	params[0]  = '-iwad';
	params[1]  = 'doom2.wad';

	startup.executable = exe;
	startup.arguments  = params;

	return process.start(startup);

}


/*================================================================
	Browse For Executables
================================================================*/
function selectExe() {
	
	var exe = air.File.documentsDirectory;
	var exeFilter = new air.FileFilter('Executables','*.exe;*.dmg;*.sh;*.bat');

	//exe.browseForOpen(air.Event.SELECT, exeSelected);

	exe.browseForOpen('Select Your Executable', new window.runtime.Array(exeFilter));
	exe.addEventListener(air.Event.SELECT, exeSelected);

	function exeSelected(e) {
		return exe.nativePath;
	}

}